﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

namespace Project
{

    public class GameController : MonoBehaviour
    {

        //public GameObject VideoPlayer;

        public GameObject PopupSubMenu;
        public static GameController Instance;
        public SpriteRenderer PopupChecker;
        public SpriteRenderer PopupChecker2;
        public SpriteRenderer PopupChecker3;
        public SpriteRenderer PopupChecker4;
        public SpriteRenderer PopupChecker5;
        public SpriteRenderer PopupChecker6;
        public SpriteRenderer PopupChecker7;
        public SpriteRenderer PopupChecker8;

        public GameObject videoFather;
        public GameObject Clip1Cristal;
        public GameObject Clip2Umburana;

        void Awake()
        {
            Instance = this;
        }

        public bool HasPopupChecker()
        {
            List<SpriteRenderer> myCheckers = new List<SpriteRenderer>() { PopupChecker, PopupChecker2, PopupChecker3, PopupChecker4, PopupChecker5, PopupChecker6, PopupChecker7, PopupChecker8 };
            foreach (SpriteRenderer spriteRenderer in myCheckers)
            {
                if (spriteRenderer.enabled)
                {
                    return true;
                }
            }
            return false;
        }

        void Update()
        {
            if (HasPopupChecker() && !PopupSubMenu.activeSelf)
            {
                PopupSubMenu.SetActive(true);
            }
            else if (!HasPopupChecker() && PopupSubMenu.activeSelf)
            {
                PopupSubMenu.SetActive(false);
            }
        }

        public void StartRoulette()
        {
            AudioManager.Instance.PlaySomething(0, 0, false);
                SceneManager.LoadScene("Roulette");
        }

        public void SetSubMenuState(bool state)
        {
            PopupSubMenu.SetActive(state);
        }

        public void SetVideoState(bool state)
        {
            AudioManager.Instance.StopAudio(1);

            if (PopupChecker.enabled || PopupChecker2.enabled || PopupChecker3.enabled || PopupChecker6.enabled || PopupChecker8.enabled)
            {
                videoFather.SetActive(state);
                videoFather.transform.GetChild(0).gameObject.SetActive(state);

                if (state)
                    videoFather.transform.GetChild(0).gameObject.GetComponent<VideoPlayer>().Play();
                else
                    videoFather.transform.GetChild(0).gameObject.GetComponent<VideoPlayer>().Stop();

            }
            else
            {
                videoFather.SetActive(state);
                videoFather.transform.GetChild(1).gameObject.SetActive(state);

                if (state)
                    videoFather.transform.GetChild(1).gameObject.GetComponent<VideoPlayer>().Play();
                else
                    videoFather.transform.GetChild(1).gameObject.GetComponent<VideoPlayer>().Stop();
            }
        }
    }
}