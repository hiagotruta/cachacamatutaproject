﻿using andlline.agenda.constantes;
using andlline.conexoes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CadastrarUsuarioDEMO : MonoBehaviour {

    [SerializeField]
    private InputField nome;
    [SerializeField]
    private InputField email;
    [SerializeField]
    private InputField cidade;
    [SerializeField]
    private InputField idade;
    [SerializeField]
    private InputField genero;
    [SerializeField]
    private InputField usuario_id;
    [SerializeField]
    private Text campoRetornoDados;


    

    //metodo de exemplo para cadastrar usuario... basta chamar esse método através de um botão
    public void Cadastra()
    {
        Usuarios.CadastrarNovoUsuario(nome.text, email.text, genero.text, cidade.text, idade.text, usuario_id.text, RetornoCadastroUsuario, this.GetType().Name);
    }
    //calback do cadastra
    private void RetornoCadastroUsuario(Usuarios.DadosRetornoUsuario dados)
    {
        //se houver sucesso no retorno dados.sucesso é true
        if (dados.sucesso)
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_DADO_INSERIDO_SUCESSO);
            nome.text = "";
            email.text = "";
            cidade.text = "";
            genero.text = "";
            idade.text = "";
            usuario_id.text = "";
        }
        else
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
        }
        ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }

    //método para a captura dos dados passando o id do usuário
    public void CapturaUsuario(InputField usuariId)
    {
        Usuarios.CapturaDadosUsuarioPorID(usuariId.text, RetornoDaCapturaDeDadosUsuario, this.GetType().Name);
    }
    //calback do capturaUsuario
    private void RetornoDaCapturaDeDadosUsuario(Usuarios.DadosRetornoUsuario dados)
    {
        if (dados.sucesso)
        {
            campoRetornoDados.text = string.Format("Nome: {0}\nEmail: {1}\nCidade: {2}\nGênero: {3}\nIdade: {4}", dados.nome, dados.email, dados.cidade, dados.genero, dados.idade);
        }
        else
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
            campoRetornoDados.text = "";
        }
        ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }

   
}
