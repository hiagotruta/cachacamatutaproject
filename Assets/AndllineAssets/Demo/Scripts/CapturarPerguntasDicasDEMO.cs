﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapturarPerguntasDicasDEMO : MonoBehaviour {

    [SerializeField]
    private InputField campoNumeroPergunta;
    [SerializeField]
    private Text resultado;


    public void CapturaPergutaDica()
    {
        int posicao = 0;
        try
        {
            posicao = int.Parse(campoNumeroPergunta.text);
        }
        catch
        {
            
        }
        PerguntasDicas.CapturaPerguntaDica(posicao, RetornoDaCapturaPerguntaDica, this.GetType().Name);
    }

    private void RetornoDaCapturaPerguntaDica(PerguntasDicas.DadosRetornoPerguntas dados)
    {
        //checar se há retorno válido
        if (dados.sucesso)
        {
            resultado.text = string.Format("Pergunta: {0}\nDica: {1}", dados.pergunta, dados.dica);
        }
        else
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
        }

        ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }
}
