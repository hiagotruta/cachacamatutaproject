﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AberturaEFechamentoSessaoDEMO : MonoBehaviour
{
    //arraste a classe usuario para ca dentro da cena... senão nao irá funcionar a contagem de permanencia
    [SerializeField]
    private Usuarios classeUsuario;




    //Recomendado chamar o abrir sessao ao dá start na primeira cena do app... e depois encerrar a sessão ao sair do app..
    //também é interessante chamar quando OnApplicationPause for true (ou seja saiu do jogo) ai vc fecha a sessa e reabre quando for false, mas que uma variável persistente for true(ou seja saiu anteriormente do jogo)
    //bom por um script como esse num objeto não destruível na primeira cena do jogo, para que esse controle seja possível.
    public const string usuario_id = "usuario_exemplo_001";
    
    private static bool sessaoFoiAberta;
  
    
    private void Start()
    {
        PlayerPrefs.SetInt("abriuSessao", 0);
        PlayerPrefs.SetString("sessao_id", "");
        Debugar.Log("no start");
        DontDestroyOnLoad(this.gameObject);
        StartCoroutine(RepetirAberturaSessao(usuario_id));

    }

    private IEnumerator RepetirAberturaSessao(string usuario_id)
    {
        while (true)
        {
            AbrirSessao(usuario_id);
            yield return new WaitForSeconds(30);
        }
    }

    //metodo que vai ficar atualizando o tempo de permanencia no app a cada 71 segundos


    private void OnApplicationPause(bool pause)
    {  //definir o usuario id como usuario_exemplo_001
        if (pause)
        {
            FecharSessao(usuario_id);
        }
        else
        {
            AbrirSessao(usuario_id);
        }
    }

    private void OnApplicationQuit()
    {
        FecharSessao(usuario_id);
    }


    //para abrir sessao, vc tem que chamar pelo usuario id... não sei como vc vai guardar isso, se em variável persistente e tal.... mas passe como parámetro nesse método


    private void AbrirSessao (string usuarioId)
    {
        Usuarios.AbrirSessao(usuarioId, RetornoAbertura, this.GetType().Name);
    }

    private void FecharSessao(string usuarioId)
    {
        Usuarios.EncerrarSessao(usuarioId, RetornoFechamento, this.GetType().Name);
    }

    private void RetornoFechamento(string[] lista)
    {
        //se lista[0].Trim()==1 então é pq deu certo, do contrário, qualquer outro resultado é para soltar um erro

        if (lista[0].Trim() == "1")
        {
            //sucesso  fazer o que quiser ou não fazer nada
            Debugar.Log("sucesso no fechamento da sessao");
        }
        else
        {
            Debugar.LogError("erro no fechamento da sessão");
        }
        ChamarLoadingBar.FinalizaBarra(GetType().Name);
    }

    //callback do abrir sessao
    private void RetornoAbertura(string[] lista)
    {
        //se lista[0].Trim()==1 então é pq deu certo, do contrário, qualquer outro resultado é para soltar um erro

        if (lista[0].Trim() == "1")
        {
            //sucesso  fazer o que quiser ou não fazer nada, mas uma coisa necessária é iniciar a corrotina de contagem 
            //de permanência apenas se for sucesso
            StartCoroutine(classeUsuario.ContaTempoPermanencia(usuario_id));

            Debugar.Log("sucesso na abertura da sessao");
        }
        else
        {
            Debugar.LogError("erro na abertura da sessão");
        }
        ChamarLoadingBar.FinalizaBarra(GetType().Name);
    }
}



