﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnviarLeituraAlvoDEMO : MonoBehaviour {

    /// <summary>
    /// Lembrar que esse alvo id é "Alvo 1" ou "Alvo 2" até 8 ... com a primeira em maíuscula e espaço entre o nome e o número
    /// </summary>



    [SerializeField]
    private InputField campoUsuario;


    [SerializeField]
    private InputField campoAlvo;

    //chamar esse método para poder enviar a leitura do alvo pelo usuário
    public void EnviarLeituraAlvo()
    {
        Usuarios.EnviaLeituraAlvo(campoUsuario.text, campoAlvo.text,Retorno, this.GetType().Name);

    }
    //para verificar se o registro no banco de dados foi realizado com sucesso o primeiro elemento da lista, o LISTA[0] tem que ser igual a 1 (string), do contrário deu algum erro.
    private void Retorno(string[] lista)
    {
        if (lista[0].Trim() == "1")
        {
            //sucesso
            //aqui deve fazer o que vc quiser... apenas para teste... vamos chamar uma caixa de diálogo
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_DADO_INSERIDO_SUCESSO);
        }
        else
        {
            //fracasso
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
        }

        //sempre lembrar de encerrar a loading bar
        ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }
}
