﻿using andlline.agenda.constantes;
using andlline.conexoes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetandoUf : MonoBehaviour
{
    [SerializeField]
    private Dropdown dpUF;
    [SerializeField]
    private Dropdown dpCidade;
    // Start is called before the first frame update
    void Start()
    {
       // ConfigGerais.Debugar = true;
        SetaOptions();
        CaptaCidade();
    }
    
    void SetaOptions()
    {
        List<string> ufs = new List<string>();
        ufs.Add("Selecione");
        ufs.Add("Acre");
        ufs.Add("Alagoas");
        ufs.Add("Amapá");
        ufs.Add("Amazonas");
        ufs.Add("Bahia");
        ufs.Add("Ceará");
        ufs.Add("Distrito Federal");
        ufs.Add("Espírito Santo");
        ufs.Add("Goiás");
        ufs.Add("Minas Gerais");
        ufs.Add("Mato Grosso");
        ufs.Add("Mato Grosso do Sul");
        ufs.Add("Pará");
        ufs.Add("Paraíba");
        ufs.Add("Pernambuco");
        ufs.Add("Piauí");
        ufs.Add("Paraná");
        ufs.Add("Rio de Janeiro");
        ufs.Add("Rio Grande do Norte");
        ufs.Add("Rondônia");
        ufs.Add("Roraima");
        ufs.Add("Rio Grande do SulS");
        ufs.Add("Santa Catarina");
        ufs.Add("Sergipe");
        ufs.Add("São Paulo");
        ufs.Add("Tocantins");
        ufs.Add("");
        dpUF.AddOptions(ufs);
    }

    public void CaptaCidade()
    {
        dpCidade.gameObject.SetActive(true);
        string sql = string.Format("select id, cidade from api_cidades where uf='{0}' order by cidade", dpUF.captionText.text);
        ConexaoJson.instance.ConexaoFormSelect(Constantes.URL_CONSULTA_EVENTOS, sql, back);
    }

    private void back(string[] lista)
    {
        List<string> cidades = new List<string>();
       // Debugar.Log("retornou ");
        if (!string.IsNullOrEmpty(lista[0]))
        {
            JsonToClass dado = null;
            foreach (string json in lista)
            {
                dado = JsonToClasseSoValido.ConverteDuasTentativas(json);
                if (dado != null)
                {
                    cidades.Add(dado.cidade);
                }
            }
            dpCidade.options.Clear();
            dpCidade.AddOptions(cidades);
        }
    }
}
