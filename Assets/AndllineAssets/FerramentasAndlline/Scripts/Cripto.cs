﻿using System;

using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace andlline.cripto
{
    /// <summary>
    ///Responsável pela Criptográfia
    /// </summary>
   
    public static class Cripto
    {



        /// <summary>
        /// Retorna o MD5 do arquivo(md5 retornado em string baseada 64 digitos)
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        //cripotagrfa 2 é a penas para manter a antiga compatibilidade
        public static string Criptografa2(this string strACrip) 
        {
            string strHash64 = "";
            try
            {

                //Passando a string para bytes
                byte[] bytesAcrip = new byte[strACrip.Length];
                for (int i = 0; i < strACrip.Length; i++)
                {
                    byte.TryParse(strACrip.Substring(i, 1).ToString(), out bytesAcrip[i]);

                }
                //Tirando o hash via md5
                MD5CryptoServiceProvider MyHash = new MD5CryptoServiceProvider();
                MyHash.ComputeHash(bytesAcrip);
                //retornando o hash em forma de string
                strHash64 = Convert.ToBase64String(MyHash.Hash);
            }
            catch { }
            return strHash64;
        }//fim de  Get_MD5_string



        public static string Criptografa(this string strACrip, string salt="")
        {
            salt = salt + ConfigGerais.SenhaSalto;
            MD5 md5Hash = MD5.Create();
            // Converte para byte
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(strACrip + salt));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        } //fim de  Get_MD5_string

    }
}
