﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using andlline.agenda.constantes;
using andlline.dialogos.barras;
using UnityEngine.SceneManagement;
using System;

public class CaixaDialogo : MonoBehaviour
{
    [SerializeField]
    private Text LabelTitulo;
    [SerializeField]
    private Text Labelmensagem;
    [SerializeField]
    private GameObject Caixa;
    [SerializeField]
    private GameObject botao1;
    [SerializeField]
    private GameObject botao2;
    [SerializeField]
    private GameObject botao3;





    public GameObject botaoE;

    //internal void Alert(object constates)
    //{
    //    throw new NotImplementedException();
    //}

    [SerializeField]
    private Text botao1Texto;
    [SerializeField]
    private Text botao2Texto;
    [SerializeField]
    private Text botao3Texto;
    [SerializeField]
    private Text botaoETexto;
    [SerializeField]
    private Text soTexto;

    //---- prompt

    [SerializeField]
    private GameObject caixaPrompt;
    [SerializeField]
    private InputField campoPrompt;
    [SerializeField]
    private GameObject btnPrompt;
    [SerializeField]
    private Text LabelTituloPrompt;
    [SerializeField]
    private Text LabelmensagemPrompt;

    public static CaixaDialogo instance;
    public static bool ativo = false;
    public delegate void funcao(string tela);
    public delegate void RetornoTextoPrompt(string entrada);
    public delegate void funcao2();
    public static bool voltavel = true;


    void Awake()
    {
        instance = this;
        Caixa.SetActive(false);
        botao1.SetActive(false);
        botao3.SetActive(false);

        botao2.SetActive(false);
        soTexto.gameObject.SetActive(false);
        ativo = false;
        voltavel = true;
        if (caixaPrompt != null)
        {
            caixaPrompt.SetActive(false);
        }
        
    }



    public void Alert(string titulo, string mensagem)
    {
        ChamaCaixa(titulo, mensagem, Constantes.TEXTO_BOTAO_NEUTRO);
        OnClickBotaoNeutro(delegate ()
        {
            Caixa.SetActive(false);
        });
        ativo = true;
    }

    public void AlertComando(string titulo, string mensagem, funcao2 comando)
    {
        ChamaCaixa(titulo, mensagem, Constantes.TEXTO_BOTAO_NEUTRO);
        OnClickBotaoNeutro(comando);
        ativo = true;
    }


    public void AlertText(string mensagem)
    {
        ChamaCaixa(mensagem);
        ativo = true;
    }
    public void CancelaTexto()
    {
        try
        {
            if (soTexto != null)
            {
                soTexto.gameObject.SetActive(false);
            }

        }
        catch { }

        ativo = false;
    }
    public void AlertConfirmacao(string titulo, string mensagem, funcao2 comandoPositivo)
    {
        Debugar.Log("alert confirmacao");
        ChamaCaixa(titulo, mensagem, Constantes.TEXTO_BOTAO_NEGATIVO, Constantes.TEXTO_BOTAO_POSITIVO);
        Debugar.Log("alert confirmacao chamou caixa");
        OnClickBotaoNegativo(delegate ()
        {
            Caixa.SetActive(false);
        });
        OnClickBotaoPositivo(delegate ()
        {
            comandoPositivo();
            CaixaDialogo.instance.Cancela();
        });
        ativo = true;
    }
    public void AlertConfirmacaoReset(string titulo, string mensagem, funcao2 comandoPositivo)
    {
        ChamaCaixa(titulo, mensagem, Constantes.TEXTO_BOTAO_NEGATIVO, Constantes.TEXTO_BOTAO_POSITIVO);
        OnClickBotaoNegativo(delegate ()
        {
            LoadingBar.instance.IniciaBarra(this.GetType().Name);
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        });
        OnClickBotaoPositivo(comandoPositivo);
        ativo = true;
    }

    public void Prompt(string titulo, string mensagem, RetornoTextoPrompt retorno, InputField.ContentType tipo= InputField.ContentType.Standard)
    {
        Caixa.SetActive(false);
        caixaPrompt.SetActive(true);
        LabelTituloPrompt.text = titulo;
        LabelmensagemPrompt.text = mensagem;
        btnPrompt.GetComponentInChildren<Text>().text = Constantes.MANUAL;
        campoPrompt.contentType = tipo;
        OnClickBotaoPrompt(delegate ()
        {
            retorno(campoPrompt.text);
            Cancela();
        });
        

    }
    public void ChamaCaixa(string mensagem)
    {
        try
        {
            LoadingBar.instance.FinalizaBarra(this.GetType().Name);
        }
        catch
        {

        }
        soTexto.gameObject.SetActive(true);
        soTexto.text = mensagem;
    }
    public void ChamaCaixa(string titulo, string mensagem, string textoBotao1, string textoBotao2, string textoBotao3)
    {
        try
        {
            LoadingBar.instance.FinalizaBarra(this.GetType().Name);
        }
        catch
        {

        }
        Caixa.SetActive(true);
        botao1.SetActive(true);
        botao3.SetActive(true);
        botao2.SetActive(true);
        // botaoE.SetActive(false);
        botao1Texto.text = textoBotao1;
        botao3Texto.text = textoBotao3;
        botao2Texto.text = textoBotao2;
        LabelTitulo.text = titulo;
        Labelmensagem.text = mensagem;




    }
    /// <summary>
    /// Botao positivo é o botao da direita. Botao negativo é o da esquerda e o neutro é o do meio
    /// </summary>
    /// <param name="titulo"></param>
    /// <param name="mensagem"></param>
    /// <param name="textoBotao1"></param>
    /// <param name="textoBotao3"></param>
    public void ChamaCaixa(string titulo, string mensagem, string textoBotao1, string textoBotao3)
    {
        try
        {
            LoadingBar.instance.FinalizaBarra(this.GetType().Name);
        }
        catch
        {

        }
        Caixa.SetActive(true);
        botao1.SetActive(true);
        botao3.SetActive(true);
        botao2.SetActive(false);
        //botaoE.SetActive(false);
        botao1Texto.text = textoBotao1;
        botao3Texto.text = textoBotao3;
        LabelTitulo.text = titulo;
        Labelmensagem.text = mensagem;

    }

    public void ChamaCaixa(string titulo, string mensagem, string textoBotao2)
    {
        try
        {
            LoadingBar.instance.FinalizaBarra(this.GetType().Name);
        }
        catch
        {

        }
        Caixa.SetActive(true);
        botao1.SetActive(false);
        botao3.SetActive(false);
        botao2.SetActive(true);
        //botaoE.SetActive(false);
        botao2Texto.text = textoBotao2;
        LabelTitulo.text = titulo;
        Labelmensagem.text = mensagem;
        Canvas.ForceUpdateCanvases();
        Canvas.ForceUpdateCanvases();
    }

    public void OnClickBotaoNegativo(funcao Funcao, string tela)
    {

        botao1.GetComponent<Button>().onClick.AddListener(() => Funcao(tela));


    }
    public void OnClickBotaoNeutro(funcao Funcao, string tela)
    {
        botao2.GetComponent<Button>().onClick.AddListener(() => Funcao(tela));


    }
    public void OnClickBotaoPositivo(funcao Funcao, string tela)
    {
        botao3.GetComponent<Button>().onClick.AddListener(() => Funcao(tela));


    }
    public void OnClickBotaoEspecial(funcao2 Funcao)
    {
        botaoE.GetComponent<Button>().onClick.RemoveAllListeners();
        botaoE.GetComponent<Button>().onClick.AddListener(() => Funcao());


    }
    public void OnClickBotaoNegativo(funcao2 Funcao)
    {
        botao1.GetComponent<Button>().onClick.RemoveAllListeners();
        botao1.GetComponent<Button>().onClick.AddListener(() => Funcao());
        //   Caixa.SetActive(false);

    }
    public void OnClickBotaoNeutro(funcao2 Funcao)
    {
        botao2.GetComponent<Button>().onClick.RemoveAllListeners();
        botao2.GetComponent<Button>().onClick.AddListener(() => Funcao());
        //  Caixa.SetActive(false);

    }
    public void OnClickBotaoPrompt(funcao2 Funcao)
    {
        if (btnPrompt != null)
        {
            btnPrompt.GetComponent<Button>().onClick.RemoveAllListeners();
            btnPrompt.GetComponent<Button>().onClick.AddListener(() => Funcao());
        }
        else
        {
            Debugar.LogError("Erro de if");
        }
       
        //  Caixa.SetActive(false);

    }
    public void OnClickBotaoPositivo(funcao2 Funcao)
    {
        botao3.GetComponent<Button>().onClick.RemoveAllListeners();
        botao3.GetComponent<Button>().onClick.AddListener(() => Funcao());
        //  Caixa.SetActive(false);

    }

    public void Cancela()
    {
        if (caixaPrompt != null)
        {
            caixaPrompt.SetActive(false);
        }
       
        Caixa.SetActive(false);
        soTexto.gameObject.SetActive(false);
        ativo = false;



    }

    public void ZeraListiners()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && voltavel)
        {
            CancelaTexto();
            Cancela();
        }
        if(!voltavel && Input.GetKeyUp(KeyCode.Escape))
        {
            voltavel = true;
        }

    }



}