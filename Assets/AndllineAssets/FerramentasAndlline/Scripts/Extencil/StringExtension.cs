﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace andlline.Extension.String
{
    public static class StringExtension
    {

        public static string TagToString(this string tag)
        {
            // return tag;
            try
            {
                string saida = "";
                if (string.IsNullOrEmpty(tag))
                {
                    return "";
                }
                saida = tag.Replace("<abrefechachaves>", "{}");
                saida = saida.Replace("<abrefechaparanteses>", "()");
                saida = saida.Replace("<abrefechacolchetes>", "[]");
                saida = saida.Replace("<aspasimples>", "'");
                saida = saida.Replace("<aspa>", "\"");
                saida = saida.Replace("<enter>", "\n");
                saida = saida.Replace("<br>", "\n");
                saida = saida.Replace("<dtraco>", "--");

                saida = saida.Replace("<abreparenteses>", "(");
                saida = saida.Replace("<fechaparenteses>", ")");
                saida = saida.Replace("<abrechave>", "{");
                saida = saida.Replace("<fechachave>", "}");
                saida = saida.Replace("<pipe>", "|");
                saida = saida.Replace("<porcento>", "%");
                //saida = saida.Replace("<b>", "");
                //saida = saida.Replace("</b>", "");
                return saida;
            }
            catch (System.Exception)
            {

                return "";
            }




        }
        public static string SoCaracteresValidos(this string texto)
        {
            string saida = Regex.Replace(texto, @"[\s\\]+", " ");
            return saida;
        }

        public static string StringToTag(this string texto)
        {
            /*
         //  return texto;
          
            string saida = "";
            if (texto != "")
            {

               // saida = texto.Replace("{}", "\\{}");
               // saida = saida.Replace("()", "\\()");
               // saida = saida.Replace("[]", "\\[]");
              //  saida = texto.Replace("'", "\\\'");
               // saida = saida.Replace("\"", "\\\"");

                saida = texto.Replace("\r\n","      ");
                saida = saida.Replace("\n", "       ");


                //saida = saida.Replace("(", @"\(");
                //saida = saida.Replace(")", @"\)");
               // saida = saida.Replace("{", @"\\\{");
               // saida = saida.Replace("}", @"\\\}");
                //saida = saida.Replace("|", @"\\|");
                return saida;
                Debugar.LogError("tag string saida " + saida);
            }
            else
            {
                return texto;
            }

            */
            string saida = "";
            if (texto != "")
            {

                saida = texto.Replace("{}", "<abrefechachaves>");
                saida = saida.Replace("()", "<abrefechaparanteses>");
                saida = saida.Replace("[]", "<abrefechacolchetes>");
                saida = saida.Replace("'", "<aspasimples>");
                saida = saida.Replace("%", "<porcento>");
                saida = saida.Replace("\"", "<aspa>");
                saida = saida.Replace("\r\n", "<br>");
                saida = saida.Replace("\r", "<br>");
                saida = saida.Replace("\n", "<br>");
                saida = saida.Replace("--", "<dtraco>");
                saida = saida.Replace("(", "<abreparenteses>");
                saida = saida.Replace(")", "<fechaparenteses>");
                saida = saida.Replace("{", "<abrechave>");
                saida = saida.Replace("}", "<fechachave>");
                saida = saida.Replace("|", "<pipe>");
                //saida = saida.Replace("<b>", "");
                //saida = saida.Replace("</b>", "");
                return saida;
            }
            else
            {
                return texto;
            }




        }
        public static string StringSize(this string texto, int size, bool reticencias = false) //define um tamanho máximo da stringe
        {
            if (size <= texto.Length && size > 0)
            {
                texto = texto.Substring(0, size);
                if (reticencias)
                {
                    texto += "...";
                }
            }

            return texto;

        }



        public static string PrimeiraMaiscula(this string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public static string removerAcentos(this string texto)
        {
            string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

            for (int i = 0; i < comAcentos.Length; i++)
            {
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }
            return texto;
        }
        public static string removerQuebraDupla(this string texto)
        {

            return texto.Replace("<br><br>", "<br>").Replace("\n\n", "\n");
        }
        public static string removerFormatacao(this string texto)
        {

            return texto.Replace("<b>", "").Replace("</b>", "");
        }

        public static string PrimeiraPalavra(this string texto)
        {
            try
            {
                return texto.Split(' ')[0];
            }
            catch
            {

                return texto;
            }

        }
        public static string PrimeiraEUltimaPalavra(this string texto)
        {
            try
            {
                string[] nomeCompleto = texto.Split(' ');
                string primeiroNome = nomeCompleto[0];
                string ultimoNome = nomeCompleto[nomeCompleto.Length - 1];
                string saida = "";
                if (primeiroNome != ultimoNome)
                {
                    saida = primeiroNome + " " + ultimoNome;
                }
                else
                {
                    saida = primeiroNome;
                }
                return saida;
            }
            catch
            {

                return texto;
            }

        }
        public static string RemoveHttp(this string texto)
        {
            texto = texto.Replace("http://", "").Replace("https://", "").Replace("Http://", "").Replace("Https://", "");
            return texto;

        }
        public static string RemoveTodasTags(this string input)
        {
            return Regex.Replace(input, "<.*?>", "");
        } 

        public static string RemoveTodasTagsMenosFormatacao(this string input)
        {
                return Regex.Replace(input, @"<(?!\/?(b|color)(?=>|\s?.*>))\/?.*?>", "");
            
        }
        public static string RemoveTodasTagsMenosFormatacaoEQuebraDeLinha(this string input)
        {
            return Regex.Replace(input, @"<(?!\/?(b|color|enter|br)(?=>|\s?.*>))\/?.*?>", "");

        }

        public static string Negrita (this string input)
        {
            input = "<b>" + input + "</b>";
            return input;
        }
            public static string RemoveTagsInvisiveis(this string texto)
        {
            //texto = texto.Replace("<Hambúrguer>", "");
            //texto = texto.Replace("<Sabor>", "");
            //texto = texto.Replace("<HotDog>", "");
            //texto = texto.Replace("<Salsicha>", "");
            //texto = texto.Replace("<Hamburguer>", "");
            //texto = texto.Replace("<Queijo>", "");
            //texto = texto.Replace("<Pão>", "");
            //texto = texto.Replace("<Molhos>", "");
            //texto = texto.Replace("<Vegetais>", "");
            //texto = texto.Replace("<Adicionais>", "");
            //texto = texto.Replace("<pedido_adicional>", "");
            //texto = texto.Replace("<Pedido>", "");
            //texto = texto.Replace("<Bebidas>", "");
            //texto = texto.Replace("<email>", "");
            //texto = texto.Replace("<nome>", "");
            //texto = texto.Replace("<fimitens>", "");
            //texto = texto.Replace("<Item>", "");
            //texto = texto.Replace("<total>", "");
            //texto = texto.Replace("<Observações>", "");
            //texto = texto.Replace("<Troco para>", "");
            //texto = texto.Replace("<Telefone>", "");
            //texto = texto.Replace("<Forma de pagamento>", "");
            //texto = texto.Replace("<Modalidade de entrega>", "");
            //texto = texto.Replace("<Modalidade de compra>", "");
            //texto = texto.Replace("<Bairro>", "");
            //texto = texto.Replace("<Cidade>", "");
            //texto = texto.Replace("<Rua>", "");
            //texto = texto.Replace("<Número>", "");
            //texto = texto.Replace("<Complemento>", "");
            //texto = texto.Replace("<uf>", "");
            //texto = texto.Replace("<UF>", "");
            //texto = texto.Replace("<cep>", "");
            //texto = texto.Replace("<CEP>", "");
            //texto = texto.Replace("<Cep>", "");
            //texto = texto.Replace("<Pedido>", "");
            //texto = texto.Replace("<Molho>", "");
            //texto = texto.Replace("<Vegetal>", "");
            //texto = texto.Replace("<Molhos>", "");
            //texto = texto.Replace("<Queijos>", "");
            //texto = texto.Replace("<Bebida>", "");
            //texto = texto.Replace("<Estabelecimento>", "");
            //texto = texto.Replace("</Pedido>", "");
            //texto = texto.Replace("</Bairro>", "");
            //texto = texto.Replace("</Cidade>", "");
            //texto = texto.Replace("</Rua>", "");
            //texto = texto.Replace("</Número>", "");
            //texto = texto.Replace("</Complemento>", "");
            //texto = texto.Replace("</uf>", "");
            //texto = texto.Replace("</UF>", "");
            //texto = texto.Replace("</Telefone>", "");
            //texto = texto.Replace("</Cep>", "");
            //texto = texto.Replace("<Endereço>", "");
            //texto = texto.Replace("</Troco para>", "");
            //texto = texto.Replace("</Forma de pagamento>", "");
            //texto = texto.Replace("</Modalidade de entrega>", "");
            //texto = texto.Replace("</Modalidade de compra>", "");
            //texto = texto.Replace("</Endereço>", "");
            //texto = texto.Replace("</Telefone>", "");
            //texto = texto.Replace("</Observações>", "");
            //texto = texto.Replace("</Hambúrguer>", "");
            //texto = texto.Replace("</Hamburguer>", "");
            //texto = texto.Replace("</Queijo>", "");
            //texto = texto.Replace("</Pão>", "");
            //texto = texto.Replace("</Molhos>", "");
            //texto = texto.Replace("</Vegetais>", "");
            //texto = texto.Replace("</Molho>", "");
            //texto = texto.Replace("</Vegetal>", "");
            //texto = texto.Replace("</Molhos>", "");
            //texto = texto.Replace("</Molhos>", "");
            //texto = texto.Replace("</Queijos>", "");
            //texto = texto.Replace("</Estabelecimento>", "");
            //texto = texto.Replace("</Bebida>", "");
            //texto = texto.Replace("</Adicionais>", "");
            //texto = texto.Replace("</pedido_adicional>", "");
            //texto = texto.Replace("</Pedido_adicional>", "");
            //texto = texto.Replace("</Bebidas>", "");
            //texto = texto.Replace("</email>", "");
            //texto = texto.Replace("</nome>", "");
            //texto = texto.Replace("</fimitens>", "");
            //texto = texto.Replace("</Item>", "");
            //texto = texto.Replace("</total>", "");
            //texto = texto.Replace("</Sabor>", "");
            //texto = texto.Replace("</HotDog>", "");
            //texto = texto.Replace("</Salsicha>", "");
            //return texto;
            return RemoveTodasTagsMenosFormatacao(texto);
        }
        public static string EnterParaTagHTML(this string texto)
        {
            try
            {
                texto = texto.Replace("\n", "<br>");
            }
            catch 
            { }
            return texto;
        }
        //extende um text
        public static string TryMoeda(this string texto)
        {
            try
            {
                return float.Parse(texto).ToString("R$ 0.00");
            }
            catch (System.Exception e)
            {
                Debugar.Log("erro de catch " + e);
                return texto;
            }
        }

        public static void MudaCoresPorTag(this Text campoTextoMudavel, string[] cores)
        {
            string texto = campoTextoMudavel.text;
            texto = texto.Replace("<Hambúrguer>", string.Format("<color={0}>", cores[0]));
            texto = texto.Replace("<Queijo>", string.Format("<color={0}>", cores[1]));
            texto = texto.Replace("<Pão>", string.Format("<color={0}>", cores[2]));
            texto = texto.Replace("<Molhos>", string.Format("<color={0}>", cores[3]));
            texto = texto.Replace("<Vegetais>", string.Format("<color={0}>", cores[4]));
            texto = texto.Replace("<Bebidas>", string.Format("<color={0}>", cores[5]));
            texto = texto.Replace("<Adicionais>", string.Format("<color={0}>", cores[6]));
            texto = texto.Replace("<pedido_adicional>", string.Format("<color={0}>", cores[6]));
            texto = texto.Replace("<Pedido>", string.Format("<color={0}>", cores[7]));
            texto = texto.Replace("<Observações>", string.Format("<color={0}>", cores[0]));
            texto = texto.Replace("<fimitens>", "");
            texto = texto.Replace("<total>", "");
            texto = texto.Replace("<nome>", "");
            texto = texto.Replace("<Item>", "");
            texto = texto.Replace("<email>", "");

            texto = texto.Replace("<Troco para>",string.Format("<size=1> <color={0}> ", cores[7]));
            texto = texto.Replace("</Troco para>", "</color></size>");
            texto = texto.Replace("<Forma de pagamento>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Forma de pagamento>", "</color></size>");
            texto = texto.Replace("<Modalidade de entrega>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Modalidade de entrega>", "</color></size>");
            texto = texto.Replace("<Modalidade de compra>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Modalidade de compra>", "</color></size>");
            texto = texto.Replace("<Endereço>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Endereço>", "</color></size>");
            texto = texto.Replace("<Telefone>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Telefone>", "</color></size>");
            texto = texto.Replace("<Bairro>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Bairro>", "</color></size>");
            texto = texto.Replace("<Cidade>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Cidade>", "</color></size>");
            texto = texto.Replace("<Rua>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Rua>", "</color></size>");
            texto = texto.Replace("<Número>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Número>", "</color></size>");
            texto = texto.Replace("<Complemento>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Complemento>", "</color></size>");
            texto = texto.Replace("<uf>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</uf>", "</color></size>");
            texto = texto.Replace("<UF>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</UF>", "</color></size>");
            texto = texto.Replace("<cep>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</cep>", "</color></size>");
            texto = texto.Replace("<CEP>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</CEP>", "</color></size>");
            texto = texto.Replace("<Cep>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Cep>", "</color></size>");
            texto = texto.Replace("<Estabelecimento>", string.Format("<size=1><color={0}>", cores[0]));
            texto = texto.Replace("</Estabelecimento>", "</color></size>");
            texto = texto.Replace("<HotDog>", string.Format("<size=1><color={0}>", cores[0]));
            texto = texto.Replace("</HotDog>", "</color></size>");
            texto = texto.Replace("<Sabor>", string.Format("<size=1><color={0}>", cores[7]));
            texto = texto.Replace("</Sabor>", "</color></size>");


















            texto = texto.Replace("</Hambúrguer>", "</color>");
            texto = texto.Replace("</Hambúrguer>", "</color>");
            texto = texto.Replace("</Queijo>", "</color>");
            texto = texto.Replace("</Pão>", "</color>");
            texto = texto.Replace("</Molhos>", "</color>");
            texto = texto.Replace("</Vegetais>", "</color>");
            texto = texto.Replace("</Bebidas>", "</color>");
            texto = texto.Replace("</Adicionais>", "</color>");
            texto = texto.Replace("</pedido_adicional>", "</color>");
            texto = texto.Replace("</Pedido>", "</color>");
            texto = texto.Replace("</Observações>", "</color>");
            texto = texto.Replace("</fimitens>", "");
            texto = texto.Replace("</total>", "");
            texto = texto.Replace("</nome>", "");
            texto = texto.Replace("</Item>", "");
            texto = texto.Replace("</email>", "");

            campoTextoMudavel.text = texto;



        }
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
        public static string ReplaceLast(this string text, string search, string replace)
        {
            int pos = text.LastIndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }


        public static string TryDecimal2CasasRetornaString(this string entrada)
        {
            try
            {
                return (decimal.Parse(entrada)).ToString("0.00");
            }
            catch (System.Exception e)
            {
                Debugar.LogError(e);
                return "-";
            }
        }

        public static string SubstituiAcentosPorCoringasMysql(this string entrada)
        {
            string acentos = "[ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç]";
            entrada = Regex.Replace(entrada, acentos, "%", RegexOptions.IgnoreCase);
            return entrada;
        }
        public static decimal TryDecimal2CasasRetornaDecimal(this string entrada)
        {
            try
            {
                return (decimal.Parse(entrada));
            }
            catch (System.Exception e)
            {
                Debugar.LogError(e);
                return -1;
            }
        }
    }
  

}


