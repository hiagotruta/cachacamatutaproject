﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
namespace andlline.dialogos.barras
{
    public class LoadingBar : MonoBehaviour {
    
    [SerializeField]
    private GameObject painel;
    public static LoadingBar instance;
    private static List<string> numeroDeBarras;
    private static List<string> numeroDeCenas;
        [SerializeField]
        private  Animator animator;


        void Awake()
    {
        instance = this;
        painel.SetActive(false);
            
    }
    // Use this for initialization
    

     void Start()
    {
            numeroDeBarras = null;
            numeroDeCenas = null;


    }

    public void IniciaBarra(string nomeMetodoClasse)
        {

            if (numeroDeBarras == null)
            {
                numeroDeBarras = new List<string>();
                numeroDeCenas = new List<string>();
            }
            else if (numeroDeCenas.Count > 0)
            {
                if (numeroDeCenas[0] != SceneManager.GetActiveScene().name)
                {
                    numeroDeBarras = null;
                    numeroDeCenas = null;
                    numeroDeBarras = new List<string>();
                    numeroDeCenas = new List<string>();


                }
            }
            
            if (!numeroDeBarras.Contains(nomeMetodoClasse))
            {
                numeroDeBarras.Add(nomeMetodoClasse);
               //Debugar.LogError("iniciada barra" + nomeMetodoClasse);
           //    Debugar.LogError("barra zero " + numeroDeBarras[0]);
           //     Debugar.LogError("qtd barras " + numeroDeBarras.Count);
                numeroDeCenas.Add(SceneManager.GetActiveScene().name);
               // foreach (string barra in numeroDeBarras)
              //  {
               //     Debugar.LogError("total de loadingbars " +barra+"  "+numeroDeBarras.Count);
              //  }
               
            }
            try
            {
                painel.SetActive(true);
            }
            catch 
            {

                
            }
           
        }
        public void FinalizaBarra(string nomeMetodoClasse)
        {
            try
            {
                if (!animator.GetBool(0))
                {
                    animator.SetBool(0, true);
                }
            }
            catch (System.Exception e)
            {

              Debugar.Log("erro da cathc "+e);
            }
           
           
            if (numeroDeBarras == null)
            {
                painel.SetActive(false); 
            }
            else 

            if (numeroDeBarras.Count >0)
                     {
                if (numeroDeBarras.Contains(nomeMetodoClasse))
                {
                    numeroDeBarras.Remove(nomeMetodoClasse);
                   // Debugar.LogError("finalizada barra" + nomeMetodoClasse + numeroDeBarras.Count);
                    if (numeroDeBarras.Count == 0)
                    {
                        painel.SetActive(false);
                        numeroDeBarras = null;
                    }
                }
               
                     }
                else
                       {
                numeroDeBarras = null;
                painel.SetActive(false);
                animator.SetBool(0, false);

            }
        }

   public void IniciaBarra()
    {
           
           // Debugar.Log("inicio barra");
            painel.SetActive(true);

        }


 public void FinalizaBarra()
    {
            //  Debugar.Log("finalizou barra");
            numeroDeBarras = null;
            animator.SetBool(0, false);

            painel.SetActive(false);
            

        }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyUp(KeyCode.Escape) && painel.gameObject.activeSelf == true)
        {
                painel.gameObject.SetActive(false);
            Debugar.Log("voltando dentro do loading bar");
            //Application.LoadLevel("menu");
        }

    }

    public static void Inicia()
    {
        instance.IniciaBarra();

    }
}

}

 