﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecebeNotificacao : MonoBehaviour {
    private static string endereco;
    private static string tabela;
    private static string usuario;
    private static string destino;
    private static string msg;
    private static string escola;
    private static int intervalo;
    private static bool vibra;
    private static bool som;
    private static int ordem = 0;

    public int OrdemLogadoStatus
    {
        get
        {
            return ordem;
        }
    }

    void Start()
    {
        //é aconselhavel que esse script esteja na primeira cena 
        //ao iniciar esse script pela primeira vez quando abra o app é preciso iniciar alguns parametros do ConfigGerais
        //qualquer alteração diferente a notificação não irá chegar
        //nome do pacote
        ConfigGerais.Pacote = "com.Besro.CacaPartage";
        //nome do final do pacote
        ConfigGerais.NOME_ESCOLA = "CacaPartage";
        //nome da intent que chama o serviço de background
        ConfigGerais.AlarmIntent = "ANDLLINE_PUSH_CacaPartage";
        //id do usuário. Vc tem que GARANTIR QUE ESSE MÉTODO SEJA CHAMADO APENAS APÓS EXISTIR UM USUARIO_ID PQ VC TEM QUE PASSAR ESSE USUARIO O EMAIL ACESSO"
        //EX.  USUARIO ID 00001
        ConfigGerais.Email_acesso = "00001";
        
        SetaPluginReceberMsg();
        
    }
    
    // Use this for initialization
    public static void SetaPluginReceberMsg () {
    if (Application.platform != RuntimePlatform.Android)
    {
        return;
    }
       
        AndroidJavaClass classeJava = new AndroidJavaClass("com.unity3d.player.UnityPlayer");


        AndroidJavaObject objetoNativo = new AndroidJavaObject("com.andlline.push.ChamaAlarme");
        Debugar.Log("objeto android ok");

        objetoNativo.CallStatic("chamaAlarme", classeJava.GetStatic<AndroidJavaObject>("currentActivity"), ConfigGerais.Parametros);
        Debugar.Log("chamaAlarme chamado");
    }
	
	

    public static void AposLogar() 
    {
       

        //é aconselhavel que esse script esteja na primeira cena 
        //ao iniciar esse script pela primeira vez quando abra o app é preciso iniciar alguns parametros do ConfigGerais
        //qualquer alteração diferente a notificação não irá chegar
        //nome do pacote
        ConfigGerais.Pacote = "com.Besro.CacaPartage";
        //nome do final do pacote
        ConfigGerais.NOME_ESCOLA = "CacaPartage";
        //nome da intent que chama o serviço de background
        ConfigGerais.AlarmIntent = "ANDLLINE_PUSH_CacaPartage";
        //id do usuário. Vc tem que GARANTIR QUE ESSE MÉTODO SEJA CHAMADO APENAS APÓS EXISTIR UM USUARIO_ID PQ VC TEM QUE PASSAR ESSE USUARIO O EMAIL ACESSO"
        //EX.  USUARIO ID 00001
        ConfigGerais.Email_acesso = "00001";

        SetaPluginReceberMsg();

    }

    public static  void AposDeslogar()
    {
        //quando deslogar o app chame esse método, vc só precisa alterar o email acesso para --- para que se desligue a notificacao;
       
        ConfigGerais.Email_acesso = "---";
        SetaPluginReceberMsg();
    }
}
