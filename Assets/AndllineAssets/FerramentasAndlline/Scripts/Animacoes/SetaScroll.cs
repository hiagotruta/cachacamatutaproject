﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetaScroll : MonoBehaviour {
    [SerializeField]
    private GameObject setaSuperior;
    [SerializeField]
    private GameObject setaInferior;
    [SerializeField]
    private Scrollbar scrollbar;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (scrollbar.value > 0.9f)
        {
            setaInferior.SetActive(true);
            setaSuperior.SetActive(false);
        }

        else if(scrollbar.value < 0.1f)
        {
            setaInferior.SetActive(false);
            setaSuperior.SetActive(true);
        }
        else
        {
            setaInferior.SetActive(true);
            setaSuperior.SetActive(true);
        }
	}
}
