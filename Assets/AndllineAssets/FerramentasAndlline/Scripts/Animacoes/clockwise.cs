﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clockwise : MonoBehaviour {
	[SerializeField]
	private Image logo;
    [SerializeField]
    private float velocidade=0.01f;
    [SerializeField]
    private float intervalo=0.05f;  

	void OnEnable(){
	
		logo=GetComponent<Image>();
		StartCoroutine("clock");
	}
		
	void OnDisable(){
		StopCoroutine("clock");
	}
	// Use this for initialization
	IEnumerator clock(){
		while(true){
			
		if(logo.fillAmount==1)
			{

		
			if(logo.fillOrigin==0){
				logo.fillOrigin=1;
			

			}else{
				logo.fillOrigin=0;
			}

				logo.fillAmount=0;

			}else
			{

				logo.fillAmount+=0.01f;

			}
		yield return new WaitForSeconds(intervalo);
		}

	}

}

