﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantesDialogo : MonoBehaviour {

    public const string TITULO_CAIXA_PADRAO = "Alerta";
    public const string MSG_DADO_INSERIDO_SUCESSO = "Dados inseridos com sucesso!";
    public const string MSG_FALHA_REGISTRO_DADO = "Erro ao inserir dados!";
}
