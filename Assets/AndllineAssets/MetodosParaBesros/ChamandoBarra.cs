﻿using andlline.dialogos.barras;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChamandoBarra : MonoBehaviour, ILoadingBars
{

    //caso queira mudar para usa própria barra , basta alterar as linhas abaixo, comentando e trocando pelo metodo que ativa e desativa sua barra
    public void FinalizaBarra(string classe)
    {
        LoadingBar.instance.FinalizaBarra(classe);
    }

    public void IniciaBarra(string classe)
    {
        LoadingBar.instance.IniciaBarra(classe);
    }
}
