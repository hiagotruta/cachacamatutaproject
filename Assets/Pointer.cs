﻿using System;
using System.Collections;
using System.Collections.Generic;
using RouletteSpace;
using UnityEngine;

public class Pointer : MonoBehaviour
{
    public GameObject[] borderLights;

    void OnTriggerEnter(Collider col)
    {
        Debug.Log(col.gameObject.name);

        StartCoroutine(WaitToCheckMission(Convert.ToInt32(col.gameObject.tag)));
    }

    IEnumerator WaitToCheckMission(int tag)
    {
        Debug.Log("wtf");
        for (int i = 0; i < borderLights.Length; i++)
        {
            borderLights[i].SetActive(true);
        }
        yield return new WaitForSeconds(2f);
        RouletteController.Instance.CheckMissionByTag(tag);
    }
}
