﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ChangeCamera : MonoBehaviour
{
    public static ChangeCamera Instance { get; set; }

    private bool camAvailable;
    private WebCamTexture frontCam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

    private bool lightOn = false;
    private bool frontCamera = false;

    private bool activeCamera;

    private void Awake()
    {
        Instance = this;
        activeCamera = false;
        //StopVuforia();
    }
    
    public void StopVuforia()
    {
        if (Vuforia.CameraDevice.Instance.IsActive())
        {
            Vuforia.CameraDevice.Instance.Deinit();
            Vuforia.CameraDevice.Instance.Stop();
        }

        FirstThing();
    }
    public void ActiveVuforia()
    {
        activeCamera = false;
        Vuforia.CameraDevice.Instance.Init();
        Vuforia.CameraDevice.Instance.Start();
    }

    public void FirstThing()
    {
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (devices[i].isFrontFacing)
            {
                frontCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (frontCam == null)
        {
            return;
        }

        frontCam.Play();
        background.texture = frontCam;
        camAvailable = true;
    }

    private void Update()
    {
        if (activeCamera)
        {
            if (!camAvailable)
                return;

            float ratio = (float)frontCam.width / (float)frontCam.height;
            fit.aspectRatio = ratio;

            float scaleY = frontCam.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1, scaleY, 1);

            int orient = -frontCam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }

    }

    public void CameraChange()
    {
        
    }
}
