﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//using andlline.api;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance { get; set; }


    private void Awake()
    {
        Instance = this;

        // PlayerPrefs.DeleteAll();

        if (PlayerPrefs.GetInt("FirstTime1") != 1)
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("FirstTime1", 1);
        }

        if (!PlayerPrefs.HasKey("UserAge"))
        {
            PopUpManager.Instance.SetPopUpQuestion("Quantos anos você tem?", "UserAge");
        }

        //else if (agePopUP.gameObject != null && PlayerPrefs.GetInt("SeeAge") == 1)
        //{
        //    agePopUP.SetActive(false);
        //    Destroy(agePopUP);
        //}

    }

    private void Start()
    {
        Guid id_user = Guid.NewGuid();
        string newid = id_user.ToString();

        PlayerStats.user_id = newid;
        PlayerPrefs.SetString("User_id", newid);

    }

    public void KnowCity()
    {

    }
  
}
