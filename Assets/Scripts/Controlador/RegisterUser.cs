﻿//using andlline.agenda.constantes;
//using andlline.conexoes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterUser : MonoBehaviour

{
    public static RegisterUser Instance { get; set; }
    [SerializeField]
    private string nome;
    [SerializeField]
    private string email;
    [SerializeField]
    private string cidade;
    [SerializeField]
    private string idade;
    [SerializeField]
    private string genero;
    [SerializeField]
    private string usuario_id;
    [SerializeField]
    private float timeUser;
    [SerializeField]
    private Text campoRetornoDados;

    private void Awake()
    {
        Instance = this;
    }

    public void UpdateData()
    {
        nome = "nãopreenchido";
        email = "nãopreenchido"; ;
        cidade = PlayerStats.cidade;
        idade = PlayerStats.idade;
        genero = "nãopreenchido"; ;
        usuario_id = PlayerStats.user_id;
        timeUser = Time.realtimeSinceStartup;
      //  TimeUpdate();
        Cadastra();
    }

    public void TimeUpdate()
    {
        //Usuarios.AtualizaSessao(email.text, (int)timeUser);
    }
    //metodo de exemplo para cadastrar usuario... basta chamar esse método através de um botão
    public void Cadastra()
    {
        Usuarios.CadastrarNovoUsuario(nome, email, genero, cidade, idade, usuario_id, RetornoCadastroUsuario, this.GetType().Name);
    }

    //calback do cadastra
    private void RetornoCadastroUsuario(Usuarios.DadosRetornoUsuario dados)
    {
        //se houver sucesso no retorno dados.sucesso é true
        if (dados.sucesso)
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_DADO_INSERIDO_SUCESSO);
            nome = "";
            email = "";
            cidade = "";
            genero = "";
            idade = "";
            usuario_id = "";
        }
        else
        {
            ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
        }
        ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }

    //método para a captura dos dados passando o id do usuário
    public void CapturaUsuario(InputField usuariId)
    {
        //Usuarios.CapturaDadosUsuarioPorID(usuariId.text, RetornoDaCapturaDeDadosUsuario, this.GetType().Name);
    }
    //calback do capturaUsuario
    private void RetornoDaCapturaDeDadosUsuario()//Usuarios.DadosRetornoUsuario dados)
    {
        //if (dados.sucesso)
        //{
        //    campoRetornoDados.text = string.Format("Nome: {0}\nEmail: {1}\nCidade: {2}\nGênero: {3}\nIdade: {4}", dados.nome, dados.email, dados.cidade, dados.genero, dados.idade);
        //}
        //else
        //{
        //    ChamarCaixaDialogo.Alert(ConstantesDialogo.TITULO_CAIXA_PADRAO, ConstantesDialogo.MSG_FALHA_REGISTRO_DADO);
        //    campoRetornoDados.text = "";
        //}
        //ChamarLoadingBar.FinalizaBarra(this.GetType().Name);
    }
}
