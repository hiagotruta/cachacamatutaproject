﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpManager : MonoBehaviour
{ 
    public static PopUpManager Instance { get; set; }

    public GameObject registerInfoObject;
    //question settings
    public GameObject popupQuestion;
    public Text titleQuestion;
    public InputField userAnswer;

    //message popUp settings
    public GameObject popUPMessage;
    public Text titleMessage;
    public Text messagePopUP;

    //city popup
    public GameObject popUPcity;
    public Text userUF;
    public Text cityUser;

    //controlador settings
    private bool isToServer;
    private string currentQuestion;

    private void Awake()
    {
        Instance = this;
    }

    public void SetPopUPCity()
    {
        registerInfoObject.SetActive(true);
        popUPcity.SetActive(true);
        popupQuestion.SetActive(false);
        popUPMessage.SetActive(false);

        isToServer = true;

        currentQuestion = "UserCity";
    }
    public void SetPopUpQuestion(string questionAsk, string pathQuestion)
    {
        if (pathQuestion.Equals("UserAge"))
            userAnswer.contentType = InputField.ContentType.IntegerNumber;
        else
            userAnswer.contentType = InputField.ContentType.Standard;

        registerInfoObject.SetActive(true);
        popupQuestion.SetActive(true);
        popUPMessage.SetActive(false);
        popUPcity.SetActive(false);

        titleQuestion.text = questionAsk;
        currentQuestion = pathQuestion;
        isToServer = true;
    }

    public void SetPopUP(string title, string message, string question)
    {
        registerInfoObject.SetActive(true);
        popupQuestion.SetActive(false);
        popUPMessage.SetActive(true);
        popUPcity.SetActive(false);

        titleMessage.text = title;
        messagePopUP.text = message;

        if (question.Equals("AgeIncorrect"))
        {
            isToServer = true;
            currentQuestion = question;
        }
        else
        {
            isToServer = false;
            currentQuestion = "";
        }
    }

    public void OkButton()
    { 
        registerInfoObject.SetActive(false);
        popupQuestion.SetActive(false);
        popUPMessage.SetActive(false);
        popUPcity.SetActive(false);

        if (isToServer)
        {
            switch (currentQuestion)
            {
                case "Name":
                    PlayerStats.userName = userAnswer.text;
                    break;
                case "Email":
                    PlayerStats.email = userAnswer.text;
                    break;
                case "UserCity":
                    string userLocal = cityUser.text + "/" + userUF.text;
                    PlayerPrefs.SetString("UserCity", userLocal);
                    PlayerStats.cidade = userLocal;
                    break;
                case "UserAge":
                    AgeBehaviour();
                    break;
                case "Genero":
                    PlayerStats.genero = userAnswer.text;
                    break;
                case "AgeIncorrect":
                    SetPopUpQuestion("Quantos anos você tem?", "UserAge");
                    break;

            }
        }
    }

    private void AgeBehaviour()
    {
        if (userAnswer.text != "")
        {
            int age = System.Convert.ToInt32(userAnswer.text);

            if (age >= 18 && age < 95)
            {
                PlayerPrefs.SetString("UserAge", userAnswer.text);
                PlayerStats.idade = userAnswer.text;
            }
            else if (age >= 95)
            {
                userAnswer.text = "";
                registerInfoObject.SetActive(false);
                popupQuestion.SetActive(false);
                popUPMessage.SetActive(false);
                popUPcity.SetActive(false);
                SetPopUP("Atenção!!", "Coloque uma idade válida", "AgeIncorrect");
            }
            else if (age < 18)
            {
                Application.Quit();
            }
        }
        else
        {
            userAnswer.text = "";
            registerInfoObject.SetActive(false);
            popupQuestion.SetActive(false);
            popUPMessage.SetActive(false);
            popUPcity.SetActive(false);
            SetPopUP("Atenção!!", "Coloque uma idade válida", "AgeIncorrect");
        }
    }
}
