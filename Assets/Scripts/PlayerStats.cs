﻿//using andlline.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using andlline.api;

public class PlayerStats : MonoBehaviour
{
    public static PlayerStats Instance { get; set; }

    public static string userName = "naopreenchido";
    public static string email = "naopreenchido";
    public static string cidade = "naopreenchido";
    public static string idade = "naopreenchido";
    public static string genero = "naopreenchido";
    public static string user_id = "naopreenchido";
    public static string timeUser = "naopreenchido";


    private void Awake()
    {
        Instance = this;

        if (PlayerPrefs.HasKey("Username"))
            userName = PlayerPrefs.GetString("Username");
        else
            userName = "Naopreenchido";

        if (PlayerPrefs.HasKey("Email"))
            email = PlayerPrefs.GetString("Email");
        else
            email = "NaoPreenchido";

        if (PlayerPrefs.HasKey("UserCity"))
            cidade = PlayerPrefs.GetString("UserCity");
        else
            cidade = "NaoPreenchido";

        if(PlayerPrefs.HasKey("UserAge"))
            idade = PlayerPrefs.GetString("UserAge");
        else
            idade= "NaoPreenchido";

        if (PlayerPrefs.HasKey("Genero"))
            genero = PlayerPrefs.GetString("Genero");
        else
            genero = "naopreenchido";

        if (PlayerPrefs.HasKey("User_id"))
            user_id = PlayerPrefs.GetString("User_id");
    }

    public void UpdateLocalInfo()
    {
        PlayerPrefs.SetString("Username", userName);
        PlayerPrefs.SetString("Email", email);
        PlayerPrefs.SetString("Cidade", cidade);
        PlayerPrefs.SetString("Idade", idade);
        PlayerPrefs.SetString("Genero", genero);

    }
    public void SendToServer()
    {
        //ConfigGerais.NOME_ESCOLA = "Matuta";

        RegisterUser.Instance.Cadastra();
        //Debug.Log("Username: " + userName + " CPF: " + cpf + " results: " + results + " obs: " + obs);

       // Usuarios.CadastrarNovoUsuario(userName, email, genero, cidade, idade, user_id, RetornoCallBack([]));
    }

    void RetornoCallBack(string[] lista)
    {
        if (lista[0] == "1")
            return;
        else
        {
            //PopUpManager.Instance.SetupPopUP("Erro ao conectar com o servidor");
        }
    }

}
