﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworksEffect : MonoBehaviour
{
    public static FireworksEffect Instance { get; set; }
    public GameObject[] Prefabs;
    private int Prefab;
    private GameObject instancePrefabOne, instancePrefabTwo, instancePrefabThree;
    private Vector3 positions;

    private void Awake()
    {
        Instance = this;
    }

    public void StartEffect()
    {
        positions = this.transform.position;
        Counter();
        StartCoroutine(TimeToNewFirework());
    }
    IEnumerator TimeToNewFirework()
    {
        yield return new WaitForSeconds(Random.Range(5, 10));
        Counter();

        //int chance = Random.Range(0, 100);
        //if (chance < 50)
        //    Counter();
        //else
        //    Counter(-1);
    }


    void Counter()
    {
        Prefab = Random.Range(0, Prefabs.Length);
        //Prefab += count;
        //if (Prefab > Prefabs.Length - 1)
        //{
        //    Prefab = 0;
        //}
        //else if (Prefab < 0)
        //{
        //    Prefab = Prefabs.Length - 1;
        //}

        if(instancePrefabOne == null)
        {
            instancePrefabOne = Instantiate(Prefabs[Prefab], positions, Quaternion.identity, this.transform);
        }
        else if (instancePrefabTwo == null)
        {
            instancePrefabTwo = Instantiate(Prefabs[Prefab], positions, Quaternion.identity, this.transform);
        }
     
        else if(instancePrefabOne != null && instancePrefabTwo != null)
        {
            int changeThis = Random.Range(0, 100);
            if(changeThis < 50)
            {
                Destroy(instancePrefabOne);
                instancePrefabOne = Instantiate(Prefabs[Prefab], positions, Quaternion.identity, this.transform);
                //instancePrefabOne.transform.localPosition = new Vector3(0, 0, 0f);
            }
            else
            {
                Destroy(instancePrefabTwo);
                instancePrefabTwo = Instantiate(Prefabs[Prefab], positions, Quaternion.identity, this.transform);
               // instancePrefabTwo.transform.localPosition = new Vector3(0, 0, 0f);
            }
       
        }

        StartCoroutine(TimeToNewFirework());

    }

    private int TakePrefabNumber()
    {
        int limit = Prefabs.Length;

        if (Prefab + 1 <= limit)
            return Prefab++;
        else
            return Prefab--;
    }
}
