﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace RouletteSpace
{
    public class RouletteController : MonoBehaviour
    {

        public Transform ArrowPivot;

        public float Speed = 100;
        public bool isSpin;
        public Collider SpinerCollider;

        public Button StartButton;
        //public Button StopButton;

        public bool CanSpin;

        public static RouletteController Instance;

        public List<InputField> PlayersInputFields;

        public List<PlayerScript> PlayersList;

        public GameObject RegisterPlayerGameObject;
        public GameObject startPanel;

        public PlayerScript CurrentPlayer;

        public int CurrentPlayerIndex = 0;

        public Text CurrentNameText;

        public GameObject MissionPopup;
        public Text MissionTitle;
        public Text MissionMessage;

        public List<GameObject> NamesGameObjects;
        public List<Text> NamesText;

        public GameObject ConfirmQuitGameObject;

        public List<Animator> Anims;
        public List<Image> ImagesLight;
        public List<Sprite> SpriteLightOn;
        public List<Sprite> SpriteLightOff;

        public SpriteRenderer LampRenderer;
        public Sprite SpriteLampOn;
        public Sprite SpriteLampOff;

        public List<Text> ScoresText;

        public Text TutorialText;
        public GameObject TutorialGameObject;

        public GameObject MissionCompletedPopup;
        public Text MissionCompletedText;

        public List<string> FailMessages;

        public GameObject AlertGameObject;
        public Text AlertText;

        public Animator BackgroundAnim;

        public Image CurrentPlayerImage;

        public Animator BgBorderAnim;

        //public GameObject RouletteGameObject;

        public void ConfirmQuitPopupState(bool state)
        {
            AudioManager.Instance.PlaySomething(1, 0, false);
            ConfirmQuitGameObject.SetActive(state);
        }

        public void QuitGame()
        {
            AudioManager.Instance.PlaySomething(1, 0, false);
            SceneManager.LoadScene("MenuScene");
        }

        void Awake()
        {
            Instance = this;
            startPanel.gameObject.SetActive(true);

            TutorialGameObject.SetActive(true);
        }

        public void StartGame()
        {
            startPanel.gameObject.SetActive(false);
            RegisterPlayerGameObject.SetActive(true);

        }

        public void Update()
        {
            if (CanSpin)
            {
                Rotate();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ConfirmQuitPopupState(true);
            }
        }

        public void SetAnim(bool state)
        {
            if (state)
            {
                BackgroundAnim.SetTrigger("Start");
                BgBorderAnim.SetTrigger("Start");
                foreach (Animator animator in Anims)
                {
                    animator.SetTrigger("Start");
                    animator.speed = 1;
                    //animator.SetTrigger("Start");
                }
                for (int i = 0; i < ImagesLight.Count; i++)
                {
                    ImagesLight[i].sprite = SpriteLightOn[i];
                }
                LampRenderer.sprite = SpriteLampOn;
            }
            else
            {
                BackgroundAnim.SetTrigger("LightOn");
                BgBorderAnim.SetTrigger("LightOn");
                foreach (Animator animator in Anims)
                {
                    animator.speed = 0;
                    //animator.SetTrigger("Stop");
                }
                for (int i = 0; i < ImagesLight.Count; i++)
                {
                    ImagesLight[i].sprite = SpriteLightOff[i];
                }
                LampRenderer.sprite = SpriteLampOff;
            }
        }

        public void SetMissionPopupState(bool state)
        {
            MissionPopup.SetActive(state);
        }

        public void RegisterPlayer()
        {
            AudioManager.Instance.PlaySomething(1, 0, false);


            foreach (InputField playersInputField in PlayersInputFields)
            {
                if (playersInputField.text != String.Empty)
                {
                    PlayersList.Add(new PlayerScript(playersInputField.text, 0));
                }
            }

            if (PlayersList.Count <= 1)
            {
                //JogadoresInsuficientes
                PlayersList.Clear();
            }
            else
            {
                RegisterPlayerGameObject.SetActive(false);
                CurrentPlayer = PlayersList[0];
                CurrentNameText.text = CurrentPlayer.Name;

                CurrentPlayerImage.GetComponent<RectTransform>().localPosition =
                    NamesGameObjects[0].GetComponent<RectTransform>().localPosition;

                TutorialText.text = CurrentPlayer.Name + ", toque na roleta para iniciar";

                for (int i = 0; i < PlayersInputFields.Count; i++)
                {
                    if (PlayersInputFields[i].text != String.Empty)
                    {
                        NamesGameObjects[i].SetActive(true);
                        NamesText[i].text = PlayersInputFields[i].text;
                        ScoresText[i].text = "0";
                    }
                    else
                    {
                        NamesGameObjects[i].SetActive(false);
                    }
                }

                StartCoroutine(WaitToShowAlert());

                //RouletteGameObject.SetActive(true);
            }
        }

        public IEnumerator StopRoulette(float time)
        {
            yield return new WaitForSeconds(time);
            SetSpinState(false);
        }

        public void DisableTutorial()
        {
            AudioManager.Instance.PlaySomething(1, 0, false);
            if (TutorialGameObject.activeSelf)
            {
                TutorialGameObject.SetActive(false);
            }
        }

        public void SetSpinState(bool state)
        {
            isSpin = state;
            if (state)
            {
                StopAllCoroutines();
                if (AlertGameObject.activeSelf)
                {
                    AlertGameObject.SetActive(false);
                }

                SetAnim(true);
                SpinerCollider.enabled = false;
                CanSpin = true;
                StartButton.interactable = false;
                //StopButton.interactable = true;

                AudioManager.Instance.PlaySomething(0, 5, true);

                StartCoroutine(StopRoulette(Random.Range(1f, 3f)));
            }
        }

        public void Rotate()
        {
            ArrowPivot.Rotate(0, 0, -Speed * Time.deltaTime);

            if (!isSpin && Speed > 0)
            {
                Stop();
            }
        }

        public void Stop()
        {
            Speed -= Random.Range(3, 4f);
            if (Speed <= 0)
            {
                SpinerCollider.enabled = true;
                Speed = Random.Range(200,650);
                CanSpin = false;
                StartButton.interactable = true;
                SetAnim(false);
                AudioManager.Instance.StopAudio(0);
            }
        }

        public void CheckMissionByTag(int tag)
        {
            SetMissionPopupState(true);
            MissionTitle.text = CurrentPlayer.Name;
            switch (tag)
            {
                case 1:
                    MissionMessage.text = "Vire uma dose de Matuta";
                    Debug.Log("Prenda 1");
                    break;
                case 2:
                    MissionMessage.text = "Faça 10 polichinelos";
                    Debug.Log("Prenda 2");
                    break;
                case 3:
                    MissionMessage.text = "Escolha uma pessoa pra beber uma dose de Matuta";
                    Debug.Log("Prenda 3");
                    break;
                case 4:
                    MissionMessage.text = "Todos bebem uma dose de Matuta";
                    Debug.Log("Prenda 4");
                    break;
                case 5:
                    MissionMessage.text = "Beba uma dose DUPLA de Matuta";
                    Debug.Log("Prenda 5");
                    break;
                case 6:
                    MissionMessage.text = "Conte uma piada para galera";
                    Debug.Log("Prenda 6");
                    break;
                case 7:
                    MissionMessage.text = "Beba um copo de água pra continuar. Próximo!";
                    Debug.Log("Prenda 7");
                    break;
                case 8:
                    MissionMessage.text = "Vamos ouvir sua voz: cante uma música";
                    Debug.Log("Prenda 8");
                    break;
                case 9:
                    MissionMessage.text = "Escolha um amigo pra beber com você uma dose de Matuta";
                    Debug.Log("Prenda 9");
                    break;
                case 10:
                    MissionMessage.text = "Dance uma música sozinho";
                    Debug.Log("Prenda 10");
                    break;
                case 11:
                    MissionMessage.text = "Fique na vontade! Sinta o aroma da cachaça Matuta, mas não beba!";
                    Debug.Log("Prenda 11");
                    break;
                case 12:
                    MissionMessage.text = "Diga EUVOUBEBERTRESDOSESAGORA, e faça o que diz!";
                    Debug.Log("Prenda 12");
                    break;
                case 13:
                    MissionMessage.text = "Faça uma continência rápido, quem não fizer com você vai beber!";
                    Debug.Log("Prenda 13");
                    break;
                case 14:
                    MissionMessage.text = "Todos bebem uma dose, menos você!";
                    Debug.Log("Prenda 14");
                    break;
            }
        }

        public void MissionCompleted()
        {
            BackgroundAnim.SetTrigger("Stop");
            BgBorderAnim.SetTrigger("Stop");
            AudioManager.Instance.PlaySomething(0, 0, false);
            CurrentPlayer.Missions++;
            ScoresText[CurrentPlayerIndex].text = CurrentPlayer.Missions.ToString();
            Debug.Log("O player " + CurrentPlayer.Name + "   fez " + CurrentPlayer.Missions + "  missões");

            if (CurrentPlayerIndex < PlayersList.Count - 1)
            {
                CurrentPlayerIndex++;
            }
            else
            {
                CurrentPlayerIndex = 0;
            }

            AudioManager.Instance.PlaySomething(0, 4, false);

            CurrentPlayer = PlayersList[CurrentPlayerIndex];
            CurrentNameText.text = CurrentPlayer.Name;

            MissionCompletedPopup.SetActive(true);
            MissionCompletedText.text = "Boa!! Agora é a vez de " + CurrentPlayer.Name;

            CurrentPlayerImage.GetComponent<RectTransform>().localPosition =
                NamesGameObjects[CurrentPlayerIndex].GetComponent<RectTransform>().localPosition;
        }

        public void MissionFail()
        {
            BackgroundAnim.SetTrigger("Stop");
            BgBorderAnim.SetTrigger("Stop");
            AudioManager.Instance.PlaySomething(1, 0, false);

            MissionCompletedPopup.SetActive(true);
            MissionCompletedText.text = CurrentPlayer.Name + " " +  FailMessages[Random.Range(0, FailMessages.Count)];

            if (CurrentPlayerIndex < PlayersList.Count - 1)
            {
                CurrentPlayerIndex++;
            }
            else
            {
                CurrentPlayerIndex = 0;
            }

            AudioManager.Instance.PlaySomething(1, 3, false);

            CurrentPlayer = PlayersList[CurrentPlayerIndex];
            CurrentNameText.text = CurrentPlayer.Name;

            CurrentPlayerImage.GetComponent<RectTransform>().localPosition =
                NamesGameObjects[CurrentPlayerIndex].GetComponent<RectTransform>().localPosition;
        }

        IEnumerator WaitToShowAlert()
        {
            int index = CurrentPlayerIndex;

            yield return new WaitForSeconds(Random.Range(5, 12));

            if (CurrentPlayerIndex == index && !isSpin)
            {
                if (TutorialGameObject.activeSelf)
                {
                    TutorialGameObject.SetActive(false);
                }
                AlertGameObject.SetActive(true);
                AlertText.text = CurrentPlayer.Name + " toque na roleta para continuar";
            }
        }

        public void CloseMissionPopup()
        {
            AudioManager.Instance.PlaySomething(1, 0, false);
            MissionCompletedPopup.SetActive(false);
            StartCoroutine(WaitToShowAlert());
        }
    }
}