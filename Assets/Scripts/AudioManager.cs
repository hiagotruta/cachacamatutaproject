﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager Instance { get; set; }

    public AudioSource[] sources;
    public AudioClip[] clips;

    private void Awake()
    {
        Instance = this;
    }

    public void PlaySomething(int indexSource, int indexClip, bool loopState)
    {
        sources[indexSource].clip = clips[indexClip];
        sources[indexSource].Play();
        sources[indexSource].loop = loopState;
    }

    public void PauseAudio(int indexSource)
    {
        //sdasd
        sources[indexSource].Pause();
    }

    public void StopAudio(int indexSource)
    {
        sources[indexSource].Stop();
    }
}
