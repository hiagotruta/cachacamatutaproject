﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using andlline;
using andlline.conexoes;

public class TrackManager : MonoBehaviour
{
    public static TrackManager Instance { get; set; }
    public GameObject ui_inicio;
    public GameObject tutorial;
    public GameObject yellowEffect, greenEffect, lataCristalEffect, lataUmburanaEffect, lataJacksonEffect;
    public GameObject photoPainel;

    [HideInInspector]
    public GameObject currentTarget;

    private GameObject currentParticle;

    private Texture2D MyTexture;

    public Transform catraOne, catraTwo;

    public GameObject cameraPhoto;

    private int[] targetReads = new int[9];
 //   private Image.PIXEL_FORMAT mPixelFormat = Image.PIXEL_FORMAT.UNKNOWN_FORMAT;
    private bool mAccessCameraImage = true;
    private bool mFormatRegistered = false;
    private int r, g, b;

    void Awake()
    {
        Instance = this;
       // CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.RGB888, true);

    }

    private void Start()
    {
        targetReads[0] = 0;
        targetReads[1] = PlayerPrefs.GetInt("Alvo 1");
        targetReads[2] = PlayerPrefs.GetInt("Alvo 2");
        targetReads[3] = PlayerPrefs.GetInt("Alvo 3");
        targetReads[4] = PlayerPrefs.GetInt("Alvo 4");
        targetReads[5] = PlayerPrefs.GetInt("Alvo 5");
        targetReads[6] = PlayerPrefs.GetInt("Alvo 6");
        targetReads[7] = PlayerPrefs.GetInt("Alvo 7");
        targetReads[8] = PlayerPrefs.GetInt("Alvo 8");


        //#if UNITY_EDITOR
        //        mPixelFormat = Image.PIXEL_FORMAT.GRAYSCALE; // Need Grayscale for Editor
        //#else
        //        mPixelFormat = Image.PIXEL_FORMAT.RGB888; // Use RGB888 for mobile
        //#endif

        // Register Vuforia life-cycle callbacks:
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
        VuforiaARController.Instance.RegisterOnPauseCallback(OnPause);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackMenu();
        }
    }

    #region PRIVATE_METHODS

    void OnVuforiaStarted()
    {

        //// Try register camera image format
        //if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        //{
        //    Debug.Log("Successfully registered pixel format " + mPixelFormat.ToString());

        //    mFormatRegistered = true;
        //}
        //else
        //{
        //    Debug.LogError(
        //        "\nFailed to register pixel format: " + mPixelFormat.ToString() +
        //        "\nThe format may be unsupported by your device." +
        //        "\nConsider using a different pixel format.\n");

        //    mFormatRegistered = false;
        //}

    }

    /// <summary>
    /// Called each time the Vuforia state is updated
    /// </summary>
    void OnTrackablesUpdated()
    {
        if (mFormatRegistered)
        {
            if (mAccessCameraImage)
            {
                //Vuforia.Image image = CameraDevice.Instance.GetCameraImage(mPixelFormat);

                //if (image != null)
                //{
                //    //Debug.Log(
                //    //    "\nImage Format: " + image.PixelFormat +
                //    //    "\nImage Size:   " + image.Width + "x" + image.Height +
                //    //    "\nBuffer Size:  " + image.BufferWidth + "x" + image.BufferHeight +
                //    //    "\nImage Stride: " + image.Stride + "\n"
                //    //);

                //    byte[] pixels = image.Pixels;
                //   // Debug.Log("Image Size: W: " + image.Width + " H: " + image.Height + " LEng: " + image);
                //    if (pixels != null && pixels.Length > 0)
                //    {
                //        r = pixels[0];
                //        g = pixels[1];
                //        b = pixels[2];
                //        //Debug.Log(
                //        //    "\nImage pixels: " +
                //        //    pixels[0] + ", " +
                //        //    pixels[1] + ", " +
                //        //    pixels[2] + ", ...\n"
                //       // );
                //    }
                //}
            }
        }
    }

    /// <summary>
    /// Called when app is paused / resumed
    /// </summary>
    void OnPause(bool paused)
    {
        if (paused)
        {
            Debug.Log("App was paused");
            UnregisterFormat();
        }
        else
        {
            Debug.Log("App was resumed");
            RegisterFormat();
        }
    }

    /// <summary>
    /// Register the camera pixel format
    /// </summary>
    void RegisterFormat()
    {
        //if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        //{
        //    Debug.Log("Successfully registered camera pixel format " + mPixelFormat.ToString());
        //    mFormatRegistered = true;
        //}
        //else
        //{
        //    Debug.LogError("Failed to register camera pixel format " + mPixelFormat.ToString());
        //    mFormatRegistered = false;
        //}
    }

    /// <summary>
    /// Unregister the camera pixel format (e.g. call this when app is paused)
    /// </summary>
    void UnregisterFormat()
    {
        //Debug.Log("Unregistering camera pixel format " + mPixelFormat.ToString());
        //CameraDevice.Instance.SetFrameFormat(mPixelFormat, false);
        //mFormatRegistered = false;
    }

    #endregion //PRIVATE_METHODS

    public void OnTrackingFound(GameObject target)
    {
        Time.timeScale = 1;

        if (currentTarget == null)
            currentTarget = target;
        else
            return;
        //Debug.Log("R: " + r + " G: " + g + " B " + b);
        //if(r > 190 && g > 170 && b  < 40)
        //{
        //    Debug.Log("Amarelo");
        //}
        //else if(r < 60 && g > 70 && b < 90)
        //{
        //    Debug.Log("verde");
        //}

        AudioManager.Instance.PlaySomething(0, 2, false);

        ui_inicio.SetActive(false);

        if (target.gameObject.name.Equals("Cristal250") || target.gameObject.name.Equals("1LitroCristal") ||
            target.gameObject.name.Equals("250Umb") || target.gameObject.name.Equals("1LUmb"))
        {
            AudioManager.Instance.PlaySomething(1, 9, true);

            //AudioManager.Instance.PlaySomething(1, 7, true);

            StartCoroutine(EnterEffect(target));
            cameraPhoto.SetActive(true);
            StartCoroutine(FireWorkManager());

            if (target.gameObject.name.Equals("250Umb") || target.gameObject.name.Equals("1LUmb"))
            {
                StartCoroutine(SetAnimationUmb());
            }

            switch (target.gameObject.name)
            {
                case "250Umb":
                    SendTargetRead(PlayerStats.user_id, "Alvo 5", 5);
                    break;
                case "1LUmb":
                    SendTargetRead(PlayerStats.user_id, "Alvo 3", 3);
                    break;
                case "Cristal250":
                    SendTargetRead(PlayerStats.user_id, "Alvo 2", 2);
                    break;
                case "1LitroCristal":
                    SendTargetRead(PlayerStats.user_id, "Alvo 8", 8);
                    break;
            }
        }

        else if (target.gameObject.name.Equals("LataCristal"))
        {
            SendTargetRead(PlayerStats.user_id, "Alvo 6", 6);

            AudioManager.Instance.PlaySomething(1, 9, true);

            //AudioManager.Instance.PlaySomething(1, 7, true);

            StartCoroutine(LataEffectManager(target));
            cameraPhoto.SetActive(true);
            StartCoroutine(FireWorkManager());

        }

        else if (target.gameObject.name.Equals("LataUmburana"))
        {
            SendTargetRead(PlayerStats.user_id, "Alvo 1", 1);

            AudioManager.Instance.PlaySomething(1, 9, true);

            //AudioManager.Instance.PlaySomething(1, 7, true);

            StartCoroutine(LataUmbsEffectManager(target));
            cameraPhoto.SetActive(true);
            StartCoroutine(FireWorkManager());
            StartCoroutine(SetAnimationUmb());
        }
        else if (target.gameObject.name.Equals("OldMatuta"))
        {
            SendTargetRead(PlayerStats.user_id, "Alvo 4", 4);

            AudioManager.Instance.PlaySomething(1, 7, true);

            StartCoroutine(OldMatutaBottleEffect(target));
            cameraPhoto.SetActive(true);
            StartCoroutine(FireWorkManager());

        }
        else if (target.gameObject.name.Equals("LataJackson"))
        {
            SendTargetRead(PlayerStats.user_id, "Alvo 7", 7);

            AudioManager.Instance.PlaySomething(1, 9, true);

            StartCoroutine(JacksonEffectManager(target));
            //FireworksEffect.Instance.StartEffect();
            cameraPhoto.SetActive(true);
            StartCoroutine(FireWorkManager());


        }
    }

    public void OnTrackingLost(GameObject target)
    {
        currentTarget = null;
        CancelInvoke();

        if (target.gameObject.name.Equals("Cristal250") || target.gameObject.name.Equals("1LitroCristal") ||
            target.gameObject.name.Equals("250Umb") || target.gameObject.name.Equals("1LUmb") || 
            target.gameObject.name.Equals("LataCristal") || target.gameObject.name.Equals("LataUmburana") ||
            target.gameObject.name.Equals("LataJackson") || target.gameObject.name.Equals("OldMatuta"))
        {
            if (currentParticle != null)
            {
                GameObject particleEffect = currentParticle;
                currentParticle = null;
                Destroy(particleEffect);
            }

            ui_inicio.SetActive(true);
            StopCoroutine(FireWorkManager());
            AudioManager.Instance.StopAudio(2);
            AudioManager.Instance.StopAudio(1);
            cameraPhoto.SetActive(false);
        }
    }

    private void SendTargetRead(string user_id, string target, int localIndex)
    {
        targetReads[localIndex]++;
        PlayerPrefs.SetInt(target, targetReads[localIndex]);
        Usuarios.EnviaLeituraAlvo(user_id, target, Retorno);
    }
    void Retorno(string[] lista)
    {
        // veririfique primeiro se a lista existe ou não é nulla

        if (!string.IsNullOrEmpty(lista[0]))
        {
            //depois verifique se o primeiro elemento é igual a string 1, aconselhavél a dar um trim para evitar caracteres não visíveis.
            if (lista[0].Trim() =="1"){
                Debug.Log("Deu certo");
                //sucesso
            }else{
                Debug.Log("Deu errado");
                //falha
            }
        }
    

    }

    IEnumerator OldMatutaBottleEffect(GameObject target)
    {
        if (currentTarget != null && target.name.Equals("OldMatuta"))
        {
            GameObject board = currentTarget.transform.Find("PlacaMatuta").gameObject;
            GameObject plantDiferecce = currentTarget.transform.Find("Plants").gameObject;

            int xStart = 1;
            Vector3 endPosition = new Vector3(0.33f, board.transform.localPosition.y, board.transform.localPosition.z);
            board.transform.localPosition = new Vector3(xStart, board.transform.localPosition.y, board.transform.localPosition.z);

            while (board.transform.localPosition.x > endPosition.x)
            {
                board.transform.localPosition = new Vector3(board.transform.localPosition.x - 0.05f,
                                                        board.transform.localPosition.y,
                                                        board.transform.localPosition.z);
                yield return new WaitForSeconds(0.05f);
            }
            AudioManager.Instance.PlaySomething(0, 8, false);
        }
    }

    IEnumerator SetAnimationUmb()
    {
        if (currentTarget != null)
        {
            GameObject girl = currentTarget.transform.Find("MatutaGirl").gameObject;
            bool value = girl.GetComponent<Animator>().GetBool("move");
            girl.GetComponent<Animator>().SetBool("move", true);
            yield return new WaitForSeconds(5);
            girl.GetComponent<Animator>().SetBool("move", false);
        }
    }

    IEnumerator JacksonEffectManager(GameObject target)
    {
        GameObject jackson_anim = target.transform.Find("JacksonManager").gameObject;

        if (target.gameObject.name.Equals("LataJackson"))
        {
            currentParticle = Instantiate(lataJacksonEffect, target.transform) as GameObject;
            currentParticle.SetActive(true);
        }

        jackson_anim.gameObject.SetActive(true);

       // Vector3 startSize = jackson_anim.transform.localScale;
       // jackson_anim.transform.localScale = Vector3.zero; //reduce size

        //while (jackson_anim.transform.localScale.x < startSize.x)
        //{
        //    jackson_anim.transform.localScale = new Vector3(jackson_anim.transform.localScale.x + 0.01f,
        //                                            jackson_anim.transform.localScale.y + 0.01f,
        //                                            jackson_anim.transform.localScale.z + 0.01f);
           
        //    yield return new WaitForSeconds(0.01f);
        //}
        yield return null;
    }

    IEnumerator LataEffectManager(GameObject target)
    {
        GameObject girl = target.transform.Find("MatutaGirl").gameObject;
        GameObject girlTwo = target.transform.Find("MatutaGirlTwo").gameObject;

        if (target.gameObject.name.Equals("LataCristal"))
        {
            currentParticle = Instantiate(lataCristalEffect, target.transform) as GameObject;
            currentParticle.SetActive(true);
        }

        girl.gameObject.SetActive(true);
        girlTwo.gameObject.SetActive(true);

        Vector3 startSize = girl.transform.localScale;
        girl.transform.localScale = Vector3.zero; //reduce size
        girlTwo.transform.localScale = Vector3.zero; //reduce size

        while (girl.transform.localScale.x < startSize.x)
        {
            girl.transform.localScale = new Vector3(girl.transform.localScale.x + 0.0025f,
                                                    girl.transform.localScale.y + 0.0025f,
                                                    girl.transform.localScale.z + 0.0025f);
            girlTwo.transform.localScale = new Vector3(girlTwo.transform.localScale.x + 0.0025f,
                                                    girlTwo.transform.localScale.y + 0.0025f,
                                                    girlTwo.transform.localScale.z + 0.0025f);
            yield return new WaitForSeconds(0.04f);
        }
        yield return null;
    }

    IEnumerator LataUmbsEffectManager(GameObject target)
    {
        GameObject girl = target.transform.Find("MatutaGirl").gameObject;
        GameObject girlTwo = target.transform.Find("MatutaGirlTwo").gameObject;

        if (target.gameObject.name.Equals("LataUmburana"))
        {
            currentParticle = Instantiate(lataUmburanaEffect, target.transform.localPosition, Quaternion.identity, target.transform) as GameObject;
            currentParticle.SetActive(true);
        }

        girl.gameObject.SetActive(true);
        girlTwo.gameObject.SetActive(true);

        Vector3 startSize = girl.transform.localScale;
        Vector3 startSizeGirlTwo = girlTwo.transform.localScale;
        girl.transform.localScale = Vector3.zero; //reduce size
        girlTwo.transform.localScale = Vector3.zero; //reduce size

        while (girl.transform.localScale.x < startSize.x)
        {
            girl.transform.localScale = new Vector3(girl.transform.localScale.x + 0.01f,
                                                    girl.transform.localScale.y + 0.01f,
                                                    girl.transform.localScale.z + 0.01f);

            if (girlTwo.transform.localScale.x < startSizeGirlTwo.x)
            {
                girlTwo.transform.localScale = new Vector3(girlTwo.transform.localScale.x + 0.01f,
                                                        girlTwo.transform.localScale.y + 0.01f,
                                                        girlTwo.transform.localScale.z + 0.01f);
            }

            yield return new WaitForSeconds(0.075f);
        }
        yield return null;
    }

    IEnumerator EnterEffect(GameObject target)
    {
        GameObject girl = target.transform.Find("MatutaGirl").gameObject;

        if (target.gameObject.name.Equals("Cristal250") || target.gameObject.name.Equals("1LitroCristal") || target.gameObject.name.Equals("Cristal250Teste"))
        {
            currentParticle = Instantiate(yellowEffect, target.transform.parent) as GameObject;
            currentParticle.SetActive(true);
        }
        else if(target.gameObject.name.Equals("250Umb") || target.gameObject.name.Equals("1LUmb"))
        {
            currentParticle = Instantiate(greenEffect, target.transform.parent) as GameObject;
            currentParticle.SetActive(true);
        }

        girl.gameObject.SetActive(true);
        Vector3 startSize = girl.transform.localScale;
        girl.transform.localScale = Vector3.zero; //reduce size

        while (girl.transform.localScale.x < startSize.x)
        {
            girl.transform.localScale = new Vector3(girl.transform.localScale.x + 0.02f,
                                                    girl.transform.localScale.y + 0.02f,
                                                    girl.transform.localScale.z + 0.02f);
            yield return new WaitForSeconds(0.05f);
        }

        yield return null;
    }

    private IEnumerator FireWorkManager()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(1, 3));
        float newSong = UnityEngine.Random.Range(0, 100);

        if (currentTarget != null)
        {
            if (newSong < 30)
                AudioManager.Instance.PlaySomething(2, 10, true);
            else if (newSong >= 30 && newSong <= 50)
                AudioManager.Instance.PlaySomething(2, 11, true);
            else if (newSong >= 50 && newSong <= 70)
                AudioManager.Instance.PlaySomething(2, 12, true);
            else if (newSong >= 70 && newSong <= 100)
                AudioManager.Instance.PlaySomething(2, 13, true);

            StartCoroutine(FireWorkManager());
        }
    }
    public void BackMenu()
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void SetTutorial(bool value)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        tutorial.SetActive(value);
    }

    public void IsPhoto(bool value)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        photoPainel.SetActive(value);
        ui_inicio.SetActive(!value);
    }
}
