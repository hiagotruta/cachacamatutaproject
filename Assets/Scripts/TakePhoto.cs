﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePhoto : MonoBehaviour
{
    public GameObject[] hideThings;
    public GameObject popUPSucess;


    private bool isReading;
    public GameObject[] objectsToEnable;
    public GameObject[] objectsDisable;

    public void SettingsAssets(bool value)
    {
        for (int i = 0; i < hideThings.Length; i++)
            hideThings[i].SetActive(value);
    }

    public void SavePhoto()
    {
        if(TrackManager.Instance.currentTarget != null)
        {
            for (int i = 0; i < objectsToEnable.Length; i++)
                objectsToEnable[i].SetActive(true);
            for (int i = 0; i < objectsDisable.Length; i++)
                objectsDisable[i].SetActive(false);
        }
        StartCoroutine(CaptureScreen());
    }

    IEnumerator CaptureScreen()
    {
        Handheld.Vibrate();

        SettingsAssets(false);

        yield return new WaitForEndOfFrame();
        string fileName = "Matuteiro";

        ScreenshotManager.SaveScreenshot(fileName, "Matuta");

        yield return new WaitForEndOfFrame();

        popUPSucess.SetActive(true);
    }

    public void ClosePopUP()
    {
        for (int i = 0; i < objectsToEnable.Length; i++)
            objectsToEnable[i].SetActive(false);
        if (TrackManager.Instance.currentTarget != null)
        {
            for (int i = 0; i < objectsDisable.Length; i++)
                objectsDisable[i].SetActive(true);
        }
        else
        {
            for (int i = 0; i < objectsDisable.Length; i++)
                objectsDisable[i].SetActive(false);
        }
        popUPSucess.SetActive(false);
        SettingsAssets(true);
    }
}