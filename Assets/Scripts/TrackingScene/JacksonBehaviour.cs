﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JacksonBehaviour : MonoBehaviour
{
    public float speed;
    private int dir;
    private Vector3 target;
    
    // Start is called before the first frame update
    void Start()
    {
        dir = 1;
        StartCoroutine(DefineTarget());
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(target);
    }

    IEnumerator DefineTarget()
    {
        int theNumber = Random.Range(0, 100);

        if(theNumber < 50)
        {
            target = new Vector3(speed * Time.deltaTime * dir, 0, 0);
        }
        else
        {
            target = new Vector3(0, 0, speed * Time.deltaTime * dir);
        }

        if (theNumber >= 50 && (this.transform.position.z >= -0.31f || this.transform.position.z <= -0.5f))
            dir *= -1;
        else
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 1));
            dir *= -1;
        }
        yield return new WaitForSeconds(Random.Range(0.5f, 1));
        StartCoroutine(DefineTarget());
    }
}
