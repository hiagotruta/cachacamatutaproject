﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using andlline;

public class MenuController : MonoBehaviour
{
    public GameObject HelpPopup;
    public GameObject tutorialPanel;
    public GameObject exitPopUP;

    public void Start()
    {
       // PopUpManager.Instance.SetPopUP("Question", "Email", "Qual o seu Email?", "");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            AudioManager.Instance.PlaySomething(0, 0, false);
            ExitApp(true);
        }
    }

    public void ExitApp(bool value)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        exitPopUP.SetActive(value);
    }

    public void ConfirmExit()
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        Application.Quit();

    }

    public void SetHelpState(bool state)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        HelpPopup.SetActive(state);
    }

    public void StartScene()
    {
        //RegisterUser.Instance.UpdateData();
        //RegisterUser.Instance.TimeUpdate();
        AudioManager.Instance.PlaySomething(0, 0, false);
        TrySendToServer();
        SceneManager.LoadScene(1);
        //SceneManager.LoadScene("Rastreamento");

    }
    public void StartGane()
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        SceneManager.LoadScene("Roulette");

    }
    public void OpenURL(string url)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        Application.OpenURL(url);
    }

    public void SetTutorial(bool value)
    {
        AudioManager.Instance.PlaySomething(0, 0, false);
        tutorialPanel.SetActive(value);
    }

    private void TrySendToServer()
    {
        try
        {
            RegisterUser.Instance.UpdateData();
            RegisterUser.Instance.TimeUpdate();
        }
        catch
        {
            Debug.Log("Error enviar pro servidor");
        }
    }
}
